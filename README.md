# 米尔行车记录仪

## 说明：

* 版本：V1.0
* 硬件 
	* 主板：米尔MYD-YT507开发板
	* 摄像：海康威视 DS-E11 USB摄像头
	* 存储：闪迪32GB高速MicroSD存储卡
* 软件
	* 系统：Ubuntu 18.0.4
	* 前端：Flutter
	* 后端：Django
	* 视频：Python+OpenCV
* 开发
	* 作者：HonestQiao



![img](%E7%B1%B3%E5%B0%94%E8%A1%8C%E8%BD%A6%E8%AE%B0%E5%BD%95%E4%BB%AA%E8%AE%BE%E8%AE%A1.png)

## 使用：

1. 下载代码：

   ```
   git clone https://gitee.com/honestqiao/MYiR-Driving-Recorder
   ```

2. 安装依赖包：

   ```
   sudo apt update
   sudo apt install python3-opencv
   sudo apt install python3-pillow
   sudo apt install python3-django
   sudo apt install python3-pip
   
   cd /path/to/MYiR-Driving-Recorder/DrivingRecorder
   pip install -r requirements.txt
   
   cd /path/to/MYiR-Driving-Recorder/backend
   pip install -r requirements.txt
   
   
   ```

3. 配置：

   ```
   # 查看摄像头：
   ls -lh /dev/video*
   # 根据显示的编号，配置DrivingRecorderAndMjpegServer.py：
   CAMERA_NO = 1
   
   # 挂载sdcard到/sdcard
   sudo mkdri /sdcard
   sudo apt install exfat-fuse
   # 查看sdcard集体设备信息
   lsblk
   sudo mount.exfat -o pagecode=936,iocharset=cp936,nonempty /dev/mmcblk0p1 /sdcard
   mkdir -p /sdcard/data/{thumbs,videos}
   ls -lh /sdcard/data/
   ln -s /sdcard/data /path/to/MYiR-Driving-Recorder/DrivingRecorder/
   ln -s /sdcard/data /path/to/MYiR-Driving-Recorder/backend/static/
   ```

4. 运行

   ```
   cd /path/to/MYiR-Driving-Recorder/DrivingRecorder
   python DrivingRecorderAndMjpegServer.py
   
   cd /sdcard/data /path/to/MYiR-Driving-Recorder/backend/
   python manage.py runserver 0.0.0.0:8000 --insecure --nostatic
   ```

5. 开发：

   * 使用Python开发DrivingRecorder和backend，各自的requirements.txt包含了依赖的包
   * 使用Flutter来开发frontend/flutterfrontend，pubspec.yaml中包含了依赖的包，将发布的web文件，同步到backend/static/web即可