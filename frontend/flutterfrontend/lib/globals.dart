// ignore_for_file: prefer_interpolation_to_compose_strings

library globals;

import 'dart:html' as html;

final hostname = html.window.location.hostname;
String serverHost = hostname.toString();
String websocketAddr = "ws://$serverHost:28889";
String restfulList = "http://$serverHost:8000/app/history_list";
String thumbDir = "http://$serverHost:8000/app/static/data/thumbs/";
String videoDir = "http://$serverHost:8000/app/static/data/videos/";
String mp4DemoUrl = "http://$serverHost:8000/static/mp4/demo.mp4";
