import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'globals.dart' as globals;
import 'websocket.dart';

class LivePage extends StatelessWidget {
  LivePage({Key? key, required this.stream}) : super(key: key);

  final Stream stream;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: FutureBuilder(
            future: null,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.all(12.0),
                child: Card(
                  child: StreamBuilder(
                    stream: stream,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return const CircularProgressIndicator();
                      }

                      if (snapshot.connectionState == ConnectionState.done) {
                        return const Center(
                          child: Text("Connection Closed !"),
                        );
                      }
                      //? Working for single frames
                      return Image.memory(
                        Uint8List.fromList(
                          base64Decode(
                            (snapshot.data.toString()),
                          ),
                        ),
                        gaplessPlayback: true,
                        excludeFromSemantics: true,
                        fit: BoxFit.scaleDown,
                      );
                    },
                  ),
                ),
              );
            }));
  }
}
