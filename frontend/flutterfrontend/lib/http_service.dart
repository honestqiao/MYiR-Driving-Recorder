import 'package:http/http.dart';
import 'dart:convert';
import 'globals.dart' as globals;
import 'video_model.dart';

class HttpService {
  final String historyURL = globals.restfulList;

  Future<List<Video>> getList() async {
    Response res = await get(Uri.parse(historyURL));

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<Video> videos = body
          .map(
            (dynamic item) => Video.fromJson(item),
          )
          .toList();

      return videos;
    } else {
      throw "Unable to retrieve videos.";
    }
  }
}
