import 'package:flutter/material.dart';
import 'globals.dart' as globals;
import 'video_model.dart';
import "video_play.dart";

class VideoDetail extends StatelessWidget {
  final Video video;

  const VideoDetail({required this.video});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(video.title),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      // ListTile(
                      //   title: const Text("Title"),
                      //   subtitle: Text(video.title),
                      // ),
                      // ListTile(
                      //   title: const Text("ID"),
                      //   subtitle: Text("${video.id}"),
                      // ),
                      // ListTile(
                      //   title: const Text("Body"),
                      //   subtitle: Text(video.body),
                      // ),
                      ListTile(
                        // title: const Text("Time"),
                        title: Text(video.time),
                      ),
                      // ListTile(
                      //   title: const Text("Video"),
                      //   subtitle: Text(video.video),
                      // ),
                      // Container(
                      //   child: new Image.network(
                      //       'http://127.0.0.1:8000/app/static/thumbs/' +
                      //           video.thumb,
                      //       fit: BoxFit.scaleDown),
                      //   color: Colors.lightBlue,
                      // ),
                      Container(
                        child: VideoPlay(video: video),
                        // color: Colors.lightBlue,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
