// import 'package:flutter/foundation.dart';

class Video {
  final int id;
  final String time;
  final String title;
  final String body;
  final String thumb;
  final String video;

  Video({
    required this.id,
    required this.time,
    required this.title,
    required this.body,
    required this.thumb,
    required this.video,
  });

  factory Video.fromJson(Map<String, dynamic> json) {
    return Video(
      id: json['id'] as int,
      time: json['time'] as String,
      title: json['title'] as String,
      body: json['body'] as String,
      thumb: json['thumb'] as String,
      video: json['video'] as String,
    );
  }
}
