import 'package:flutter/material.dart';
import 'globals.dart' as globals;

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text(
          "米尔行车记录仪\n\n版本：V1.0\n\n硬件\n主板：米尔MYD-YT507开发板\n摄像：海康威视 DS-E11 USB摄像头\n存储：闪迪32GB高速MicroSD存储卡\n\n\n软件\n系统：Ubuntu 18.0.4\n前端：Flutter\n后端：Django\n视频：Python+OpenCV\n\n\n开发\n作者：HonestQiao"),
    );
  }
}
