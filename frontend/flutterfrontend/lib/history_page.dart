import 'package:flutter/material.dart';
import 'globals.dart' as globals;
import 'http_service.dart';
import 'video_detail.dart';
import 'video_model.dart';

class HistoryPage extends StatelessWidget {
  HistoryPage({Key? key}) : super(key: key);
  final HttpService httpService = HttpService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("历史记录"),
      // ),
      body: FutureBuilder(
        future: httpService.getList(),
        builder: (BuildContext context, AsyncSnapshot<List<Video>> snapshot) {
          if (snapshot.hasData) {
            List<Video> videos = snapshot.data ?? <Video>[];
            return ListView(
              children: videos
                  .map(
                    (Video video) => ListTile(
                      leading: Image(
                        image: NetworkImage(globals.thumbDir + video.thumb),
                      ),
                      title: Text(video.title),
                      // subtitle: Text(video.time),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                      dense: true,
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => VideoDetail(
                            video: video,
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
