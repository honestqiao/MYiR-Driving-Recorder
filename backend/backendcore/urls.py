from django.urls import path
from . import views
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('hellodjango', views.hello_django, name = "hellodjango"),
    path('history_list', views.history_list, name = "history_list")
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()